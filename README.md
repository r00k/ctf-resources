# CTF Resources

## Key Take Aways  
* (Good) CTF challenges emulate real life problems. Learn from them.  
* Documentation is important in all things. Take good notes (even if you're racing).  
* If you're playing to win, time management is key. Block out the whole time.  
* If you're playing to learn, slow down, take your time. Do a write up (or video).  
  
## Finding free CTFs
**CTFTime** - https://ctftime.org/  
**Holiday Hack Challenge** - https://holidayhackchallenge.com/past-challenges/  
  
## CTF-like Challenges
**Hack the Box** - https://www.hackthebox.eu/  
**OverTheWire** - https://overthewire.org/  
**VulnHub** - https://www.vulnhub.com/  

## CTF Groups
**OpenToAll** - https://opentoallctf.github.io/  
**BrakeSec Security** - https://www.brakeingsecurity.com/  
**HTB Discord** - https://discord.gg/hRXnCFA  
**John Hammond CTF Discord** - https://discord.gg/Kgtnfw4  
  
## Other CTF Resources
https://github.com/apsdehal/awesome-ctf  
